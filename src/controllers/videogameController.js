const videoGame = require('./../models/VideoGame')

module.exports = {
        // Cadastro de Video-Game
        async postVideoGame(req, res) {
            try {
    
    
                const videogame = await videoGame.create(req.body)
               
                return res.status(200).json({sucess: true, data:videogame})
    
            } catch (error) {
    
                res.status(400).json({ message: 'Error no cadastro de video-game' })
            }
    
        },
        // Lista de Video-Game
        async getVideoGame(req, res) {
            try {
                const videogame = await videoGame.find({}, { name: 1, company: 1 })
                return res.status(200).json(videogame)
            } catch (error) {
                res.status(400).json({ message:'Error na Lista de video-game' })
            }
        },
    
    
    
}