const videoGame = require('../models/VideoGame')
const jogos = require('../models/Jogos')


module.exports = {



    // Cadastro de Jogos 
    async postJogos(req, res) {
        try {

            const { id } = req.params;
            const videogames = await videoGame.findById(id);
            const { jogo_name, console_name, console_id } = req.body
            const { _id, name } = videogames
            const jogo = await jogos.create({ jogo_name, console_name: name, console_id: _id })
            return res.status(200).json({
                sucess: true,
                data: jogo
            })

        } catch (error) {
            res.status(400).json({ message: 'Error no Cadastro de jogod' })

        }




    },

    // Lista de Jogos
    async getJogos(req, res) {
        try {

            const jogo = await jogos.find({})
            return res.status(200).json(jogo)
        } catch (error) {
            res.status(400).json({ message: "Error lista de jogos" })
        }
    },


    // Visualizar  JOGOS/CONSOLE
    async getJogoConsole(req, res) {
        try {

            const jogo = await jogos.find({}, { jogo_name: 1, console_name: 1 })
            return res.status(200).json(jogo)
        } catch (error) {
            res.status(400).json({ message: "Error na lista de jogos-consoles" })
        }
    },

    // Pesquisar jogos por console
    async searchGetConsole(req, res) {
        try {
            const console_game = req.params.searchConsole
            const { console_name } = req.body

            const jogo = await jogos.find({ console_name: `${console_game}` })

            if (jogo === null) {
                return res.status(400).json({ message: 'Valor nulo' })

            }
             else {

                return res.status(200).json(jogo)
            }

        } catch (error) {
            res.status(400).json({
                message: "Error na lista de pesquisa por console"
            })
        }
    },

    // Pesquisar Jogos por nome
    async searchGetJogo(req, res) {
        try {
            const game = req.params.searchJogo
            const { jogo_name } = req.body

            const jogo = await jogos.find({ jogo_name: game })
            if (jogo === null) {
                return res.status(400).json({ message: 'Valor nulo' })

            } else {

                return res.status(200).json(jogo)
            }

        } catch (error) {
            res.status(400).json({
                message: "Error na lista de pesquisa por jogos"
            })
        }
    }




}
