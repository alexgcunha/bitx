const express = require('express')
const routes = express.Router()

const jogoControllers = require('./controllers/jogoControllers')
const videoControllers = require('./controllers/videogameController')

//Rotas do VIDEO GAME
routes.get('/video-game', videoControllers.getVideoGame)
routes.post('/video-game', videoControllers.postVideoGame)


//Rotas de JOGOS
routes.post('/:id/jogo', jogoControllers.postJogos)
routes.get('/jogo', jogoControllers.getJogos)
routes.get('/jogo-console/:searchConsole', jogoControllers.searchGetConsole)
routes.get('/jogo/:searchJogo', jogoControllers.searchGetJogo)
routes.get('/jogo-console', jogoControllers.getJogoConsole)





module.exports = routes