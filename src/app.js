require('dotenv').config();
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')

// Variavel da PORTA DO SERVIDOR E DO BANCO DE DADOS
const { PORT, MONGODB } = process.env


// Iniciando Express
const app = express()


// Conexão do banco de dados
mongoose.connect(MONGODB, { useNewUrlParser: true })

app.use(cors())

// Aceitando Json
app.use(express.json())

// Rotas da api
app.use(require('./routes'));

//Servidor
app.listen(PORT, () => {
    console.log(`Servidor rodando na ${PORT}` )
})